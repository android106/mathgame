package com.withiphong.mathgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.withiphong.mathgame.databinding.FragmentMinusgameBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MinusGameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MinusGameFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentMinusgameBinding
    private var answer = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_minusgame, container, false)
        val args = MinusGameFragmentArgs.fromBundle(requireArguments())
        var correct = args.correct
        var incorrect = args.incorrect
            binding.apply {
                setScore(correct, incorrect)
                generateQuestion()

                // CheckAnswer
                btnAns1.setOnClickListener {
                    val checkAns = btnAns1.text.toString()
                    if (checkAns.toInt() == answer) {
                        Toast.makeText(activity, "correct", Toast.LENGTH_LONG).show()
                        correct++
                        textCorrect.text = getString(R.string.scoreCorrect, correct)
                    } else {
                        Toast.makeText(activity, "incorrect", Toast.LENGTH_LONG).show()
                        incorrect++
                        textIncorrect.text = getString(R.string.scoreIncorrect, incorrect)
                    }
                    generateQuestion()
                }

                btnAns2.setOnClickListener {
                    val checkAns = btnAns2.text.toString()
                    if (checkAns.toInt() == answer) {
                        Toast.makeText(activity, "correct", Toast.LENGTH_LONG).show()
                        correct++
                        textCorrect.text = getString(R.string.scoreCorrect, correct)
                    } else {
                        Toast.makeText(activity, "incorrect", Toast.LENGTH_LONG).show()
                        incorrect++
                        textIncorrect.text = getString(R.string.scoreIncorrect, incorrect)
                    }
                    generateQuestion()
                }

                btnAns3.setOnClickListener {
                    val checkAns = btnAns3.text.toString()
                    if (checkAns.toInt() == answer) {
                        Toast.makeText(activity, "correct", Toast.LENGTH_LONG).show()
                        correct++
                        textCorrect.text = getString(R.string.scoreCorrect, correct)
                    } else {
                        Toast.makeText(activity, "incorrect", Toast.LENGTH_LONG).show()
                        incorrect++
                        textIncorrect.text = getString(R.string.scoreIncorrect, incorrect)
                    }
                    generateQuestion()
                }
            }

        requireActivity().onBackPressedDispatcher.addCallback(this){
            view?.findNavController()?.navigate(MinusGameFragmentDirections.actionMinusGameFragmentToTitleFragment(correct,incorrect))
        }

        return binding.root
    }

    private fun FragmentMinusgameBinding.generateQuestion() {
        val randomNum1 = (0..10).random()
        val randomNum2 = (0..10).random()
        num1.text = randomNum1.toString()
        num2.text = randomNum2.toString()
        val ans = randomNum1 - randomNum2
        answer = ans
        generateAnswer()
    }

    private fun FragmentMinusgameBinding.generateAnswer() {
        val answers = arrayOf(
            "btnAns1", "btnAns2", "btnAns3"
        )
        val btnRandom = answers[(0..2).random()]
        if (btnRandom == "btnAns1") {
            btnAns1.text = answer.toString()
            btnAns2.text = (-10..10).random().toString()
            btnAns3.text = (-10..10).random().toString()
        } else if (btnRandom == "btnAns2") {
            btnAns1.text = (-10..10).random().toString()
            btnAns2.text = answer.toString()
            btnAns3.text = (-10..10).random().toString()
        } else {
            btnAns1.text = (-10..10).random().toString()
            btnAns2.text = (-10..10).random().toString()
            btnAns3.text = answer.toString()
        }
    }

    private fun FragmentMinusgameBinding.setScore(
        correct: Int,
        incorrect: Int
    ) {
        textCorrect.text = getString(R.string.scoreCorrect, correct)
        textIncorrect.text = getString(R.string.scoreIncorrect, incorrect)
    }

}