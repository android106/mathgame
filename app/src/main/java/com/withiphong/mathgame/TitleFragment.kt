package com.withiphong.mathgame

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.withiphong.mathgame.databinding.FragmentTitleBinding
import kotlinx.android.synthetic.main.fragment_title.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [titleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class titleFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(inflater, R.layout.fragment_title, container, false)
        val args = titleFragmentArgs.fromBundle(arguments!!)
        var correct = args.correct
        var incorrect = args.incorrect

        binding.apply {
            textPointCorrect.text = getString(R.string.scoreCorrect, correct)
            textPointIncorrect.text = getString(R.string.scoreIncorrect, incorrect)
        }

        binding.playPlusGameButton.setOnClickListener { view ->
          view.findNavController().navigate(titleFragmentDirections.actionTitleFragmentToPlusGameFragment(correct,incorrect))
        }
        binding.playMultiGameButton.setOnClickListener { view ->
            view.findNavController().navigate(titleFragmentDirections.actionTitleFragmentToMultiplyGameFragment(correct,incorrect))
        }
        binding.playMinusGameButton.setOnClickListener { view ->
            view.findNavController().navigate(titleFragmentDirections.actionTitleFragmentToMinusGameFragment(correct,incorrect))
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!, view!!.findNavController()) || super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.option_menu, menu)
    }
}